#!/usr/bin/env python2
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

from algo import buildGraphic

def dataToZ(name):
    data = []
    dataset = pd.read_csv(name+'.csv', sep=';', comment='#', header=-1)
    for rI,v in dataset.iterrows():
        row = []
        for i in dataset.iloc[rI].values:
            row.append(i)
        data.append(row)
    return data 

Z1 = dataToZ("data")
Z1 = np.array(Z1)
print (Z1.shape)
X = np.arange(0, 1, 1.0/Z1.shape[1])
Y = np.arange(0, 1, 1.0/Z1.shape[0])
buildGraphic(X, Y, Z1)

Z2 = dataToZ("data2")
Z2 = np.array(Z2)
buildGraphic(X, Y, Z2)

Z3 = dataToZ("data3")
Z3 = np.array(Z3)
buildGraphic(X, Y, Z3)
