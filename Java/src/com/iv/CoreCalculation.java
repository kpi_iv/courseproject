package com.iv;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CoreCalculation {

    private int maxX = 1;
    private int minX = 0;
    private int maxT = 1;
    private int minT = 0;

    public double[][] getAccurateCalculation(Mathematic mainMath, int nodeXCount, int nodeTCount) {
        double[][] result = new double[nodeXCount][nodeTCount];

        double stepX = Double.valueOf(maxX - minX) / nodeXCount;
        double stepT = Double.valueOf(maxT - minT) / nodeTCount;

        for (int t = 0; t < nodeTCount; t++) {
            for (int x = 0; x < nodeXCount; x++) {
                result[x][t] = mainMath.getAccurateFunctionValue(x * stepX, t * stepT);
            }
        }

        return result;

    }

    public double[][] getApproximateCalculatioSeries(Mathematic mainMath, int nodeXCount, int nodeTCount) {
        double[][] result = new double[nodeXCount][nodeTCount];

        double stepX = Double.valueOf(maxX - minX) / nodeXCount;
        double stepT = Double.valueOf(maxT - minT) / nodeTCount;

        for (int t = 0; t < nodeTCount; t++) {
            for (int x = 0; x < nodeXCount; x++) {
                if (t == 0) {
                    result[x][t] = mainMath.getAccurateFunctionValue(x * stepX, t * stepT);
                } else if (x == 0 || x == nodeXCount - 1) {
                    result[x][t] = mainMath.getAccurateFunctionValue(x * stepX, t * stepT);
                } else {
                    double wik = result[x][t - 1];
                    double wip1k = result[x + 1][t - 1];
                    double wim1k = result[x - 1][t - 1];
                    double wikp1 = mainMath.getApproximateFunctionValue(wik, wip1k, wim1k, stepX, stepT);
                    result[x][t] = wikp1;
                }
            }
        }
        return result;
    }

    public double[][] getApproximateCalculatioParallel(Mathematic mainMath, int nodeXCount, int nodeTCount) {
        double[][] result = new double[nodeXCount][nodeTCount];

        double stepX = Double.valueOf(maxX - minX) / nodeXCount;
        double stepT = Double.valueOf(maxT - minT) / nodeTCount;

        CompletableFuture fillFirstTimeLayer, secondFuture, resultFuture;

        IntStream.range(0, nodeXCount)
                .parallel()
                .forEach(x -> result[x][0] = mainMath.getAccurateFunctionValue(x * stepX, 0));

        for (int t = 1; t < nodeTCount; t++) {
            int finalT = t;
            IntStream.range(0, nodeXCount)
                    .parallel()
                    .forEach(x -> {
                        if (x == 0 || x == nodeXCount - 1) {
                            result[x][finalT] = mainMath.getAccurateFunctionValue(x * stepX, finalT * stepT);
                        } else {
                            double wik = result[x][finalT - 1];
                            double wip1k = result[x + 1][finalT - 1];
                            double wim1k = result[x - 1][finalT - 1];
                            double wikp1 = mainMath.getApproximateFunctionValue(wik, wip1k, wim1k, stepX, stepT);
                            result[x][finalT] = wikp1;
                        }
                    });
        }
        return result;
    }

}
