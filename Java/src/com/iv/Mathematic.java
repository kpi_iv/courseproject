package com.iv;

public class Mathematic {

    public double a;
    public double b;

    public double C;

    Mathematic(double a, double b, double C) throws Exception{
        if(a<-1 || a == 0){
            throw new Error("a param is invalide");
        }
        if(b == 0){
            throw new Error("b param is invalide");
        }
        if(b/a>=0){
            throw new Error("b and a proportion is invalide");
        }
        if(C == 0){
            throw new Error("C param is invalide");
        }
        this.a = a;
        this.b = b;
        this.C = C;
    }


    public double getAccurateFunctionValue(double x, double t) {

        double addend1 = -1*Math.sqrt(-b/a);
        double addend2 = C* Math.exp((-5*a*t)/6+Math.sqrt((a+1)/6)*x);

        Double res = Math.pow(addend1+ addend2,-2);

        if(res.isInfinite())
        {
            res = Double.MAX_VALUE;
            res = Double.valueOf(100000);
        }

        return res;
    }


    public double getApproximateFunctionValue(double wik, double wip1k, double wim1k, double h, double t) {

        double dw_dx = (wip1k - wim1k)/2*h;

        double d2w_d2x = (wim1k - 2*wik + wip1k)/Math.pow(h,2);

        double addend1 = d2w_d2x;

        double addend2 = a* wik;

        double addend3 = b* Math.pow(wik,2);


        Double res = (addend1 + addend2 + addend3)*t + wik;

        if(res.isInfinite())
        {
            res = Double.MAX_VALUE;
            res = Double.valueOf(10000000000.0);
        }

        return res;
    }
}
