package com.iv;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws Exception {
        Mathematic mainMath = new Mathematic(2, -15, 1);
        CoreCalculation Core = new CoreCalculation();

        Timer _timer = new Timer();

        long duration1, duration2, duration3;

        int nodeXCount = 10;
        int nodeTCount = 400;

        _timer.setStartTime();
        double[][] accurateResult = Core.getAccurateCalculation(mainMath, nodeXCount, nodeTCount);
        _timer.setEndTime();
        duration1 = _timer.getDuration();

        _timer.setStartTime();
        double[][] aproximateSeriesResult = Core.getApproximateCalculatioSeries(mainMath, nodeXCount, nodeTCount);
        _timer.setEndTime();
        duration2 = _timer.getDuration();

        _timer.setStartTime();
        double[][] aproximateParallelResult = Core.getApproximateCalculatioParallel(mainMath, nodeXCount, nodeTCount);
        _timer.setEndTime();
        duration3 = _timer.getDuration();

        System.out.println("Algorithm duration:");
        System.out.println("Accurate duration:     " + duration1);
        System.out.println("Aproximate@1 serial:   " + duration2);
        System.out.println("Aproximate@1 parallel: " + duration3);

        showErrors(accurateResult, aproximateSeriesResult);

        DecimalFormat df = new DecimalFormat("0.00");
        showTable(accurateResult, 4, 10, df);
        showTable(aproximateSeriesResult, 4, 10, df);
        showTable(aproximateParallelResult, 4, 10, df);

        saveToCSV(accurateResult, "data.csv");
        saveToCSV(aproximateSeriesResult, "data2.csv");
        saveToCSV(aproximateParallelResult, "data3.csv");

        LaunchPython();


    }

    private static void showTable(double[][] matrix, int row, int column, DecimalFormat format) {

        System.out.println("Matrix: " + row + "/" + column);
        for (int i = 0; i < matrix.length; i += matrix.length / (row)) {

            String matrixRow = "";
            for (int j = 0; j < matrix[i].length; j += matrix[i].length / (column)) {
                matrixRow += format.format(matrix[i][j]) + " ";
            }
            System.out.println(matrixRow);
            System.out.print("\n");
        }
    }

    private static void showErrors(double[][] accurateResult, double[][] aproximateSeriesResult) {
        System.out.println("\nAlgorithm errors:");

        double absoluteError = ErrorCalculator.getMaxAbsoluteError(accurateResult, aproximateSeriesResult);
//        absoluteError = Math.round(absoluteError * 100000) / 100000.0;
        System.out.println("Max absolute error: " + absoluteError);

        double relativeError = ErrorCalculator.getMaxRelativeError(accurateResult, aproximateSeriesResult);
//        relativeError = Math.round(relativeError * 100000) / 100000;
        System.out.println("Max relative error: " + relativeError + "%");
    }

    private static void saveToCSV(double[][] data, String name) throws IOException {
        String patch = "../PyPlot/" + name;
        FileWriter writer = new FileWriter(patch, false);

        for (int x = 0; x < data.length; x++) {
            for (int t = 0; t < data[x].length; t++) {
                writer.write(Double.toString(data[x][t]));
                if (t != data[x].length - 1)
                    writer.write(';');
            }
            writer.write('\n');
        }
        writer.flush();
    }

    private static void LaunchPython() throws IOException, InterruptedException {

        ProcessBuilder pb = new ProcessBuilder("python", "main.py");
        pb.directory(new File("../PyPlot/"));
        Process p = pb.start();

        p.waitFor();
    }
}
