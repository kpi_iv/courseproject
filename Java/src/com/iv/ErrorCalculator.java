package com.iv;

public class ErrorCalculator {

    public static double getMaxAbsoluteError(double[][] accurateResult, double[][] result) {

        double maxError = 0;
        for (int i = 0; i < accurateResult.length; i++) {
            for (int j = 0; j < accurateResult[i].length; j++) {
                double newError = Math.abs(accurateResult[i][j] - result[i][j]);
                maxError = Math.max(maxError, newError);
            }
        }
        return  maxError;
    }

    public static double getMaxRelativeError(double[][] accurateResult, double[][] result) {
        double maxError = 0;
        for (int i = 0; i < accurateResult.length; i++) {
            for (int j = 0; j < accurateResult[i].length; j++) {
                double newError = Math.abs(accurateResult[i][j] - result[i][j]) / accurateResult[i][j];
                newError*=100;
                maxError = Math.max(maxError, newError);
            }
        }
        return  maxError;
    }
}
