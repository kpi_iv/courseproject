package com.iv;

public class Timer {

    long startTime;
    long endTime;

    public Timer() {
        this.startTime = 0;
        this.endTime = 0;
    }

    public long getDuration() {
        return endTime - startTime;
    }

    public void setStartTime() {
        this.startTime = System.nanoTime();

    }

    public void setEndTime() {
        this.endTime = System.nanoTime();
    }
}
